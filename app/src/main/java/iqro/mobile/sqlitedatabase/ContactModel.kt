package iqro.mobile.sqlitedatabase

import androidx.room.Entity

/**
 *Created by Zohidjon Akbarov
 */
data class ContactModel(val id:Int? = 0,val name:String,val phoneNumber:String,val address:String,val age:Int)
